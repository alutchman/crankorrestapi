package org.apps.base01.crankapi.interfacing;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

import org.apps.base01.crankapi.interfacing.JsonCrnkCoreData;

@Data
public class JsonCrnkFullData {
    private JsonCrnkCoreData data;
    private Map<String,String> links = new HashMap<>();
}
