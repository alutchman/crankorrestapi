package org.apps.base01.crankapi;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Getter
@PropertySource("classpath:application.yaml")
@Component
public class FeedBackInfo {
    @Value("${data.urlinfo}")
    private String urlInfo;

    @Value("${server.port}")
    private int port;

}
