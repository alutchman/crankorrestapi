package org.apps.base01.crankapi;

import io.crnk.validation.ValidationModule;
import lombok.extern.slf4j.Slf4j;
import org.apps.base01.helperutils.config.DataBaseConfiguration;
import org.apps.base01.helperutils.config.SpringSecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;


@SpringBootApplication
@Import({ DataBaseConfiguration.class, SpringSecurityConfig.class})
@ComponentScan({"org.apps.base01.crankapi","org.apps.base01.helperutils.controllers"})
@Slf4j
public class CrankexampleApplication {


    @Bean
    public ValidationModule validationModule(){
        return ValidationModule.create(false);
    }

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(CrankexampleApplication.class, args);
        FeedBackInfo feedBackInfo = context.getBean(FeedBackInfo.class);
        log.info("Visit {} in your browser",feedBackInfo.getUrlInfo());
        log.info("Port = {} ",feedBackInfo.getPort());
    }
}
