package org.apps.base01.crankapi.interfacing;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class JsonCrnkCoreLinks {
    private Map<String,String> links = new HashMap<>();
}
