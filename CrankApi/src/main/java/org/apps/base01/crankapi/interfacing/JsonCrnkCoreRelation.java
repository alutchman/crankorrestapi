package org.apps.base01.crankapi.interfacing;

import lombok.Data;

@Data
public class JsonCrnkCoreRelation {
    private JsonCrnkCoreLinks roles;
}
