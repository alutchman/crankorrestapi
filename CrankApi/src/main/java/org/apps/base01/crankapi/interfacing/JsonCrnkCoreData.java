package org.apps.base01.crankapi.interfacing;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class JsonCrnkCoreData {
    private String id;
    private String type;
    private Map<String,String> links = new HashMap<>();
    private Map<String,Object> attributes = new HashMap<>();
    private JsonCrnkCoreRelation relationships;
}
