package org.apps.base01.crankapi.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.crnk.core.engine.registry.RegistryEntry;
import io.crnk.core.engine.registry.ResourceRegistry;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;


@AllArgsConstructor
@RestController
public class CrankController {
    private ResourceRegistry resourceRegistry;

    private ObjectMapper objectMapper;

    @PostConstruct
    public void configureJackson() {
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        objectMapper.setDateFormat(sdf);
   }

    @RequestMapping("/resources-info")
    public Map<String, String> getResources() {
        Map<String, String> result = new HashMap<>();
        // Add all resources
/*        for (RegistryEntry entry : resourceRegistry.getResources()) {
            String key = entry.getResourceInformation().getResourceType();
            if (key.startsWith("meta/")) {
                continue;
            }
            result.put(entry.getResourceInformation().getResourceType(), resourceRegistry.getResourceUrl(entry.getResourceInformation()));
        }*/
        return result;
    }
}