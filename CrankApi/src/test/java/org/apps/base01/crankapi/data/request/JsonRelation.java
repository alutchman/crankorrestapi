package org.apps.base01.crankapi.data.request;

public class JsonRelation {
    private Object id;

    private String type;

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
