package org.apps.base01.crankapi.data.request;

import java.util.HashMap;
import java.util.Map;

public class JsonCrankData extends JsonRelation {

    private Map<String,Object> attributes = new HashMap<>();

    private Map<String,JsonRelationData> relationships  = new HashMap<>();

    private Map<String,String> links = new HashMap<>();

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public Map<String, JsonRelationData> getRelationships() {
        return relationships;
    }

    public void setRelationships(Map<String, JsonRelationData> relationships) {
        this.relationships = relationships;
    }

    public Map<String, String> getLinks() {
        return links;
    }

    public void setLinks(Map<String, String> links) {
        this.links = links;
    }
}
