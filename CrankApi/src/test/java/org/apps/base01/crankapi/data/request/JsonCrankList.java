package org.apps.base01.crankapi.data.request;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonCrankList {
    private List<Map<String,Object>> data;

    private Map<String,String> links = new HashMap<>();

    public Map<String, String> getLinks() {
        return links;
    }

    public void setLinks(Map<String, String> links) {
        this.links = links;
    }

    public List<Map<String, Object>> getData() {
        return data;
    }

    public void setData(List<Map<String, Object>> data) {
        this.data = data;
    }


}
