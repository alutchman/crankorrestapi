package org.apps.base01.crankapi.data.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "type" })
public class JsonCranckRequest {
    private JsonCrankData data = new JsonCrankData();

    public JsonCrankData getData() {
        return data;
    }

    public void setId(Object id) {
        data.setId(id);
    }

    public void setType(String type) {
        data.setType(type);
    }

    public String getType() {
        return data.getType();
    }

    public void setvalue(String key , Object value) {
        data.getAttributes().put(key, value);
    }

    public void updateRelation(String ident, JsonRelationData jsonRelationData) {
        getData().getRelationships().put(ident,jsonRelationData);
    }
}
