package org.apps.base01.crankapi.data.request;

public class JsonRelationData {
    private JsonRelation data = new JsonRelation();

    public JsonRelation getData() {
        return data;
    }

    public void setData(JsonRelation data) {
        this.data = data;
    }

}
