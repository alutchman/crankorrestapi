package org.apps.base01.crankapi.data.request;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class JsonCrnkFilterResponse {
    private List<JsonCrnkFilterItem> data = new ArrayList<>();
    private Map<String,String> links = new HashMap<>();
}
