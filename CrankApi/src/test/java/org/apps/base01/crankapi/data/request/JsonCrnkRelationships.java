package org.apps.base01.crankapi.data.request;

import lombok.Data;

@Data
public class JsonCrnkRelationships {
    private JsonCrnkRoles roles;
}
