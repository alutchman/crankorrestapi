package org.apps.base01.crankapi.data.request;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class JsonCrnkRoles {
    private Map<String,String> links = new HashMap<>();
}
