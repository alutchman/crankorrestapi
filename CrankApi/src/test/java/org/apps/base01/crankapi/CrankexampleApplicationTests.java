package org.apps.base01.crankapi;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@SpringBootTest
public class CrankexampleApplicationTests {
    @Test
    public void contextLoads() {
        log.info("Test context loaded successfully");
    }

}
