package org.apps.base01.crankapi.data.utils;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apps.base01.crankapi.data.request.JsonCranckRequest;
import org.apps.base01.crankapi.data.request.JsonCrankList;
import org.apps.base01.crankapi.data.request.JsonRelationData;
import org.apps.base01.crankapi.interfacing.JsonCrnkFullData;
import org.apps.base01.helperutils.interfacing.RestResponseHandler;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@Slf4j
public class CreateTestData {
    public static void createMovie(String baseUrl, String title, int year, List<String> actors, RestTemplate restTemplate) {
        UUID newId = UUID.nameUUIDFromBytes(title.getBytes());
        log.info("Type={}  naam={}  id={}  ","movie", title, newId.toString());

        String uriget = baseUrl + "/api/movie";

        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("user", "password"));
        ResponseEntity<JsonCrankList> response = restTemplate.exchange(uriget, HttpMethod.GET, entity, JsonCrankList.class);
        assertNotNull(response.getBody());
        assertEquals(200, response.getStatusCodeValue());

        JsonCrankList resultData = response.getBody();

        for (Map<String,Object> movieItem : resultData.getData()) {
            String id = (String) movieItem.get("id");

            if (UUID.fromString(id).equals(newId)) {
                return;
            }
        }

        JsonCranckRequest jsonCranckRequest0 = new JsonCranckRequest();
        jsonCranckRequest0.getData().setId(newId);
        jsonCranckRequest0.getData().setType("movie");
        jsonCranckRequest0.getData().getAttributes().put("year",year);
        jsonCranckRequest0.getData().getAttributes().put("name",title);
        jsonCranckRequest0.getData().getAttributes().put("version",4);
        String uri =   baseUrl + "/api/"+ jsonCranckRequest0.getType();
        HttpEntity<JsonCranckRequest> entity0 = new HttpEntity(jsonCranckRequest0,
                RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrnkFullData> response0 = restTemplate.exchange(uri, HttpMethod.POST, entity0, JsonCrnkFullData.class);
        assertEquals(201, response0.getStatusCodeValue());

        int nummer = 1;
        for (String actor : actors) {
            UUID personid = UUID.nameUUIDFromBytes(actor.getBytes());

            JsonRelationData jsonPersonDataNew = new JsonRelationData();
            jsonPersonDataNew.getData().setId(personid);
            jsonPersonDataNew.getData().setType("person");

            JsonRelationData jsonMovie = new JsonRelationData();
            jsonMovie.getData().setId(newId);
            jsonMovie.getData().setType("movie");
            createRole(baseUrl, "actor nr:"+ nummer++,
                    "actor: "+ actor, jsonPersonDataNew, jsonMovie, restTemplate);
        }
    }


    public static JsonCrnkFullData createRole(String baseUrl, String name, String description, JsonRelationData jsonPersonDataNew, JsonRelationData jsonMovie, RestTemplate restTemplate){
        JsonCranckRequest jsonCranckRequest = new JsonCranckRequest();
        jsonCranckRequest.getData().setType("role");
        jsonCranckRequest.getData().getAttributes().put("role", name);
        jsonCranckRequest.getData().getAttributes().put("description", description);

        jsonCranckRequest.getData().getRelationships().put("person",jsonPersonDataNew);
        jsonCranckRequest.updateRelation("person",jsonPersonDataNew);
        jsonCranckRequest.updateRelation("movie",jsonMovie);

        String uri2 =  baseUrl+ "/api/"+ jsonCranckRequest.getType();

        HttpEntity<JsonCranckRequest> entity0 = new HttpEntity(jsonCranckRequest,
                RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrnkFullData> response0 = restTemplate.exchange(uri2, HttpMethod.POST, entity0, JsonCrnkFullData.class);
        assertEquals(201, response0.getStatusCodeValue());
        return response0.getBody();
    }

    public static  JsonCrnkFullData createPerson(String baseUrl, String title, RestTemplate restTemplate) {
        UUID uid = UUID.nameUUIDFromBytes(title.getBytes());
        log.info("Type={}  naam={}  id={}  ","person", title, uid.toString());

        String uriget = baseUrl+"/api/person";

        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("user", "password"));
        ResponseEntity<JsonCrankList> response = restTemplate.exchange(uriget, HttpMethod.GET, entity, JsonCrankList.class);
        assertNotNull(response.getBody());
        assertEquals(200, response.getStatusCodeValue());

        JsonCrankList resultData = response.getBody();

        for (Map<String,Object> personItem : resultData.getData()) {
            String id = (String) personItem.get("id");
            if (UUID.fromString(id).equals(uid)) {
                return null;
            }
        }

        JsonCranckRequest jsonCranckRequest = new JsonCranckRequest();
        jsonCranckRequest.setId(uid);
        jsonCranckRequest.setType("person");
        jsonCranckRequest.setvalue("name",title);
        String uri =   baseUrl+"/api/"+ jsonCranckRequest.getType();

        HttpEntity<JsonCranckRequest> entity0 = new HttpEntity(jsonCranckRequest,
                RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrnkFullData> response0 = restTemplate.exchange(uri, HttpMethod.POST, entity0, JsonCrnkFullData.class);
        assertEquals(201, response0.getStatusCodeValue());

        log.info("Creating person {} with status {}", title, response.getStatusCodeValue());

        return response0.getBody();
    }

    public static JsonCrnkFullData createAdres(String baseUrl, String postcode, int nummer, String toevoeging,
                                   String woonplaats, String straatnaam, RestTemplate restTemplate){
        String uriget = baseUrl+"/api/address";
        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("user", "password"));
        ResponseEntity<JsonCrankList> response = restTemplate.exchange(uriget, HttpMethod.GET, entity, JsonCrankList.class);
        assertNotNull(response.getBody());
        assertEquals(200, response.getStatusCodeValue());

        JsonCrankList resultData = response.getBody();

        for (Map<String,Object> adresItem : resultData.getData()) {
            Map<String,Object> attrib = (Map<String, Object>) adresItem.get("attributes");
            Integer nummerChk = (Integer) attrib.get("number");
            String zipcode = (String) attrib.get("zipcode");
            String ext = (String) attrib.get("extension");

            if (zipcode.equals(postcode) && nummerChk.equals(nummer)  && ext.equals(toevoeging)) {
                return null;
            }
        }

        JsonCranckRequest jsonCranckRequest = new JsonCranckRequest();
        jsonCranckRequest.setType("address");
        jsonCranckRequest.setvalue("zipcode",postcode);
        jsonCranckRequest.setvalue("number",nummer);
        jsonCranckRequest.setvalue("extension",toevoeging);
        jsonCranckRequest.setvalue("city", woonplaats);
        jsonCranckRequest.setvalue("streetname","straatnaam");

        String uri = baseUrl+"/api/"+ jsonCranckRequest.getType();

        HttpEntity<JsonCranckRequest> entity0 = new HttpEntity(jsonCranckRequest,
                RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrnkFullData> response0 = restTemplate.exchange(uri, HttpMethod.POST, entity0, JsonCrnkFullData.class);
        assertEquals(201, response0.getStatusCodeValue());

        log.info("Creating location {} with status {}", postcode, response.getStatusCodeValue());

        return response0.getBody();
    }
}
