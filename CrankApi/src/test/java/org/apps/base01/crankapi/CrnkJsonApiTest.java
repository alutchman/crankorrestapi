package org.apps.base01.crankapi;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import org.apps.base01.crankapi.data.request.JsonCranckRequest;
import org.apps.base01.crankapi.data.request.JsonCrankList;
import org.apps.base01.crankapi.data.request.JsonCrnkFilterResponse;
import org.apps.base01.crankapi.data.request.JsonRelationData;
import org.apps.base01.crankapi.interfacing.JsonCrnkCoreData;
import org.apps.base01.crankapi.interfacing.JsonCrnkCoreLinks;
import org.apps.base01.crankapi.interfacing.JsonCrnkFullData;
import org.apps.base01.helperutils.interfacing.RestResponseHandler;
import org.apps.base01.helperutils.responses.ErrorResponse;
import org.apps.base01.helperutils.utils.GenericResult;
import org.apps.base01.helperutils.utils.JsonConvertUtility;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;
import static org.apps.base01.crankapi.data.utils.CreateTestData.createAdres;
import static org.apps.base01.crankapi.data.utils.CreateTestData.createMovie;
import static org.apps.base01.crankapi.data.utils.CreateTestData.createPerson;
import static org.apps.base01.crankapi.data.utils.CreateTestData.createRole;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;


@Slf4j
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CrnkJsonApiTest {
    private static final int PORTNUMBER = 65531;
    private static final String PORTARG = "--server.port=" + PORTNUMBER;
    private static final String BASE_URL = "http://localhost:" + PORTNUMBER;

    private static boolean isInitialized;

    private static RestTemplate restTemplate = RestResponseHandler.getRestTemplate();

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @BeforeClass
    public static void init() {
        if (!isInitialized) {
            String[] args = {PORTARG};
            CrankexampleApplication.main(args);

            createPerson(BASE_URL, "Ben Affleck", restTemplate);
            createPerson(BASE_URL, "Anna Kendrick", restTemplate);
            createPerson(BASE_URL, "Robert Downey Jr.", restTemplate);
            createPerson(BASE_URL, "Stan Lee", restTemplate);
            createPerson(BASE_URL, "Jeff Bridges", restTemplate);
            createPerson(BASE_URL, "Brad Pitt", restTemplate);
            createPerson(BASE_URL, "Angelina Jolie", restTemplate);
            createPerson(BASE_URL, "Leonardo DiCaprio", restTemplate);
            createPerson(BASE_URL, "Kate Winslet", restTemplate);
            createPerson(BASE_URL, "Terrence Howard", restTemplate);
            createPerson(BASE_URL, "Klient Iestwoet", restTemplate);

            createMovie(BASE_URL, "The Accountant", 2016, Arrays.asList("Ben Affleck", "Anna Kendrick"), restTemplate);
            createMovie(BASE_URL, "Iron Man", 2008, Arrays.asList("Robert Downey Jr.", "Terrence Howard", "Jeff Bridges"), restTemplate);
            createMovie(BASE_URL, "Titanic", 1997, Arrays.asList("Leonardo DiCaprio", "Kate Winslet"), restTemplate);
            createMovie(BASE_URL, "Mr. & Mrs. Smith", 2005, Arrays.asList("Brad Pitt", "Angelina Jolie"), restTemplate);
            createAdres(BASE_URL, "2907DD", 11, "", "Capelle aan den IJssel", "Grafiek", restTemplate);
            createMovie(BASE_URL, "To Be or Not to Be", 1911, new ArrayList<>(), restTemplate);

            createRole(BASE_URL, "no outer link", "actor missing ", null, null, restTemplate);
            isInitialized = true;
        }
    }

    @Test
    public void testResource() {
        final String uri = BASE_URL + "/api";

        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("user", "password"));
        ResponseEntity<JsonCrnkCoreLinks> response = restTemplate.exchange(uri, HttpMethod.GET, entity, JsonCrnkCoreLinks.class);
        assertNotNull(response);
        JsonCrnkCoreLinks jsonCrnkCoreLinks = response.getBody();
        assertNotNull(jsonCrnkCoreLinks);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(jsonCrnkCoreLinks.getLinks().get("address"));
        assertNotNull(jsonCrnkCoreLinks.getLinks().get("browse"));
        assertNotNull(jsonCrnkCoreLinks.getLinks().get("facet"));
        assertNotNull(jsonCrnkCoreLinks.getLinks().get("meta"));
        assertNotNull(jsonCrnkCoreLinks.getLinks().get("movie"));
        assertNotNull(jsonCrnkCoreLinks.getLinks().get("person"));
        assertNotNull(jsonCrnkCoreLinks.getLinks().get("presentation"));
        assertNotNull(jsonCrnkCoreLinks.getLinks().get("role"));
    }

    @Test
    public void testBrowse() {
        final String uri = BASE_URL + "/api/browse/";
        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("user", "password"));
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        assertNotNull(response);
        assertNotNull(response.getBody());
        assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    public void testMeta() {
        final String uri = BASE_URL + "/api/meta/";
        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("user", "password"));
        ResponseEntity<JsonCrnkCoreLinks> response = restTemplate.exchange(uri, HttpMethod.GET, entity, JsonCrnkCoreLinks.class);
        assertNotNull(response);
        JsonCrnkCoreLinks jsonCrnkCoreLinks = response.getBody();
        assertNotNull(jsonCrnkCoreLinks);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(jsonCrnkCoreLinks.getLinks().get("arrayType"));
        assertNotNull(jsonCrnkCoreLinks.getLinks().get("dataObject"));
        assertNotNull(jsonCrnkCoreLinks.getLinks().get("jsonObject"));
        assertNotNull(jsonCrnkCoreLinks.getLinks().get("resource"));
        assertNotNull(jsonCrnkCoreLinks.getLinks().get("collectionType"));
        assertNotNull(jsonCrnkCoreLinks.getLinks().get("element"));
    }


    @Test
    public void testMetaJson() {
        final String uri = BASE_URL + "/api/meta/jsonObject";
        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("user", "password"));
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        assertNotNull(response);
        assertNotNull(response.getBody());
        assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    public void testResourceInfo() {
        final String uri = BASE_URL + "/resources-info";
        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("user", "password"));
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        assertNotNull(response);
        assertNotNull(response.getBody());
        assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    public void testMovie() {
        final String uri = BASE_URL + "/api/movie";
        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("user", "password"));
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
        assertNotNull(response.getBody());
        assertEquals(200, response.getStatusCodeValue());
        log.info(response.getBody());
    }

    @Test
    public void testMovieMetAndMrsSmith() {
        final String uri = BASE_URL + "/api/movie/44cda6d4-1118-3600-9cab-da760bfd678c";
        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("user", "password"));
        ResponseEntity<JsonCrnkFullData> response = restTemplate.exchange(uri, HttpMethod.GET, entity, JsonCrnkFullData.class);
        assertNotNull(response.getBody());
        assertEquals(200, response.getStatusCodeValue());
        log.info(response.getBody().toString());
        JsonCrnkFullData data = response.getBody();
        assertEquals("Mr. & Mrs. Smith", data.getData().getAttributes().get("name"));
        assertEquals(2005, data.getData().getAttributes().get("year"));
    }

    @Test
    public void testRole() {
        final String uri = BASE_URL + "/api/role";
        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("user", "password"));
        ResponseEntity<JsonCrnkCoreData> response = restTemplate.exchange(uri, HttpMethod.GET, entity, JsonCrnkCoreData.class);
        assertNotNull(response.getBody());
        assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    public void testPerson() {
        final String uri = BASE_URL + "/api/person";
        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("user", "password"));
        ResponseEntity<JsonCrnkCoreData> response = restTemplate.exchange(uri, HttpMethod.GET, entity, JsonCrnkCoreData.class);
        assertNotNull(response.getBody());
        assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    public void testSchedule() {
        final String uri = BASE_URL + "/api/schedule";

        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<ErrorResponse> response = restTemplate.exchange(uri, HttpMethod.GET, entity, ErrorResponse.class);
        assertNotNull(response.getBody());
        ErrorResponse errorResponse = response.getBody();

        assertNotNull(errorResponse);
        assertEquals(404, errorResponse.getStatus().intValue());
        assertEquals("No message available", errorResponse.getMessage());
        assertEquals("Not Found", errorResponse.getError());
        assertEquals("/api/schedule", errorResponse.getPath());
    }


    @Test
    public void testDeleteOK() {
        String title = "Iron Man";
        UUID delId = UUID.nameUUIDFromBytes(title.getBytes());
        final String uriGet = String.format(BASE_URL + "/api/movie/%s/roles", delId.toString());

        log.info("Movie id={} wil be deleted", delId.toString());

        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrankList> response = restTemplate.exchange(uriGet, HttpMethod.GET, entity, JsonCrankList.class);
        assertEquals(200, response.getStatusCodeValue());

        JsonCrankList resultData = response.getBody();

        for (Map<String, Object> rolitem : resultData.getData()) {
            String rolToDel = (String) rolitem.get("id");
            final String uriDelRol = String.format(BASE_URL + "/api/role/%s", rolToDel);
            log.info(uriDelRol);
            HttpEntity entity0 = new HttpEntity(RestResponseHandler.createHeadersCrnk("admin", "password"));
            ResponseEntity<JsonCrankList> response0 = restTemplate.exchange(uriDelRol, HttpMethod.DELETE, entity0, JsonCrankList.class);
            assertEquals(204, response0.getStatusCodeValue());
        }

        String movieDelUrl = String.format(BASE_URL + "/api/movie/%s", delId.toString());
        log.info(movieDelUrl);
        HttpEntity entity0 = new HttpEntity(RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrankList> response0 = restTemplate.exchange(movieDelUrl, HttpMethod.DELETE, entity0, JsonCrankList.class);
        assertEquals(204, response0.getStatusCodeValue());
    }

    @Test
    public void searchMoviewFoundOne() {
        String title = "The Accountant";
        String moviesUrl = BASE_URL + "/api/movie?filter[name]=" + title;

        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrnkFilterResponse> response = restTemplate.exchange(moviesUrl, HttpMethod.GET, entity, JsonCrnkFilterResponse.class);
        assertEquals(200, response.getStatusCodeValue());

        JsonCrnkFilterResponse data = response.getBody();
        assertNotNull(data);
        assertEquals(200, response.getStatusCodeValue());
    }


    @Test
    public void searchMoviewFoundNone() {
        String title = "The Undef Movie";
        String moviesUrl = BASE_URL + "/api/movie?filter[name]=" + title;

        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrnkFilterResponse> response = restTemplate.exchange(moviesUrl, HttpMethod.GET, entity, JsonCrnkFilterResponse.class);
        assertEquals(200, response.getStatusCodeValue());

        JsonCrnkFilterResponse data = response.getBody();
        assertNotNull(data);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals(0, data.getData().size());
    }


    @Test
    public void searchMoviewFoundMore() {
        String title = "T%";

        String movieDelUrl = BASE_URL + "/api/movie?filter[name][like]=" + title;

        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrnkFilterResponse> response = restTemplate.exchange(movieDelUrl, HttpMethod.GET, entity, JsonCrnkFilterResponse.class);

        JsonCrnkFilterResponse data = response.getBody();
        assertNotNull(data);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals(3, data.getData().size());

        assertEquals("Titanic", data.getData().get(0).getAttributes().get("name"));
        assertEquals("To Be or Not to Be", data.getData().get(1).getAttributes().get("name"));
        assertEquals("The Accountant", data.getData().get(2).getAttributes().get("name"));
    }

    @Test
    public void testDeleteRole1() {
        String movieDelUrl = String.format(BASE_URL + "/api/role/%d", 2);

        log.info(movieDelUrl);
        HttpEntity entity0 = new HttpEntity(RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrankList> response0 = restTemplate.exchange(movieDelUrl, HttpMethod.DELETE, entity0, JsonCrankList.class);
        assertEquals(204, response0.getStatusCodeValue());
    }


    @Test
    public void postNewMovieJson() {
        String title = "Lady Hawk";
        UUID uuid = UUID.nameUUIDFromBytes(title.getBytes());
        String uri = BASE_URL + "/api/movie";

        String dataNew = String.format("{\n" +
                "  \"data\" : {\n" +
                "    \"id\"   : \"%s\", \n" +
                "    \"type\" : \"movie\",\n" +
                "    \"attributes\" : {\n" +
                "      \"year\" : 1984,\n" +
                "      \"name\" : \"%s\",\n" +
                "      \"version\" : 2\n" +
                "    }\n" +
                "  }\n" +
                "}", uuid.toString(), title);

        HttpEntity<String> entity0 = new HttpEntity(dataNew, RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrnkFullData> response0 = restTemplate.exchange(uri, HttpMethod.POST, entity0, JsonCrnkFullData.class);
        assertEquals(201, response0.getStatusCodeValue());
    }

    @Test
    public void postNewMovieObject() {
        String uri = BASE_URL + "/api/movie";
        String title = "Fantasy Island";
        UUID uuid = UUID.nameUUIDFromBytes(title.getBytes());

        JsonCranckRequest jsonCranckRequest = new JsonCranckRequest();
        jsonCranckRequest.getData().setId(uuid);
        jsonCranckRequest.getData().setType("movie");
        jsonCranckRequest.getData().getAttributes().put("year", 1987);
        jsonCranckRequest.getData().getAttributes().put("name", title);
        jsonCranckRequest.getData().getAttributes().put("version", 4);

        HttpEntity<JsonCranckRequest> entity0 = new HttpEntity(jsonCranckRequest, RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrnkFullData> response0 = restTemplate.exchange(uri, HttpMethod.POST, entity0, JsonCrnkFullData.class);
        assertEquals(201, response0.getStatusCodeValue());

        JsonCrnkFullData body = response0.getBody();
        assertEquals(1987, body.getData().getAttributes().get("year"));
    }

    @Test
    public void postNewLocatie() {
        JsonCranckRequest jsonCranckRequest = new JsonCranckRequest();
        jsonCranckRequest.setType("address");
        jsonCranckRequest.setvalue("zipcode", "2907DD");
        jsonCranckRequest.setvalue("number", 18);
        jsonCranckRequest.setvalue("extension", "");
        jsonCranckRequest.setvalue("city", "Capelle aan den Ijssel");
        jsonCranckRequest.setvalue("streetname", "Grafiek");

        String uri = BASE_URL + "/api/" + jsonCranckRequest.getType();

        HttpEntity<JsonCranckRequest> entity0 = new HttpEntity(jsonCranckRequest, RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrnkFullData> response0 = restTemplate.exchange(uri, HttpMethod.POST, entity0, JsonCrnkFullData.class);
        assertEquals(201, response0.getStatusCodeValue());
        assertNotNull(response0.getBody());
    }


    @Test
    public void testPatchAsdresString() {
        String uri = BASE_URL + "/api/address/1";

        String jsonBody = "{\n" +
                "  \"data\" : {\n" +
                "    \"type\" : \"address\",\n" +
                "    \"attributes\" : {\n" +
                "      \"zipcode\" : \"2907DD\",\n" +
                "      \"phone\" : \"061236789\",\n" +
                "      \"streetname\" : \"Grafiek\",\n" +
                "      \"number\" : 11,\n" +
                "      \"extension\" : \"\",\n" +
                "      \"city\" : \"Capelle aan den Ijssel\"\n" +
                "    }\n" +
                "  }\n" +
                "}";

        log.info("Patch data with : " + jsonBody);

        HttpEntity<String> entity0 = new HttpEntity(jsonBody, RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrnkFullData> response0 = restTemplate.exchange(uri, HttpMethod.PATCH, entity0, JsonCrnkFullData.class);
        assertEquals(200, response0.getStatusCodeValue());
        assertNotNull(response0.getBody());
        JsonCrnkFullData patchResponse = response0.getBody();
        assertEquals("061236789", (String) patchResponse.getData().getAttributes().get("phone"));
        assertEquals("2907DD", (String) patchResponse.getData().getAttributes().get("zipcode"));
        assertEquals("Grafiek", (String) patchResponse.getData().getAttributes().get("streetname"));
    }

    @Test
    public void testPatchAsdresWithObjectWithRestTemplate() {
        String uri = BASE_URL + "/api/address/1";
        String newPhone = "0104561010";

        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("user", "password"));
        ResponseEntity<JsonCrnkFullData> response = restTemplate.exchange(uri, HttpMethod.GET, entity, JsonCrnkFullData.class);
        assertNotNull(response.getBody());
        assertEquals(200, response.getStatusCodeValue());

        JsonCrnkFullData orgData = response.getBody();
        assertNotNull(orgData);
        String OldPhone = (String) orgData.getData().getAttributes().get("phone");
        assertNotEquals(newPhone, OldPhone);

        orgData.getData().getAttributes().put("phone", newPhone);

        String jsonBody = JsonConvertUtility.getJsonAsString(orgData);
        log.info("Data to patch : {}", jsonBody);

        HttpEntity<String> entity0 = new HttpEntity(jsonBody, RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrnkFullData> response0 = restTemplate.exchange(uri, HttpMethod.PATCH, entity0, JsonCrnkFullData.class);
        assertEquals(200, response0.getStatusCodeValue());
        assertNotNull(response0.getBody());
        JsonCrnkFullData patchResponse = response0.getBody();
        assertEquals(newPhone, patchResponse.getData().getAttributes().get("phone"));
    }

    @Test
    public void addRole() {
        String naamVanPersoon = "Klient Iestwoet";
        UUID uid = UUID.nameUUIDFromBytes(naamVanPersoon.getBytes());
        String uri0 = BASE_URL + "/api/person/" + uid.toString();

        String movieNaam = "To Be or Not to Be";
        UUID uidMovie = UUID.nameUUIDFromBytes(movieNaam.getBytes());


        JsonRelationData jsonPersonDataNew = new JsonRelationData();
        jsonPersonDataNew.getData().setId(uid);
        jsonPersonDataNew.getData().setType("person");

        JsonRelationData jsonMovie = new JsonRelationData();
        jsonMovie.getData().setId(uidMovie);
        jsonMovie.getData().setType("movie");

        JsonCranckRequest jsonCranckRequest = new JsonCranckRequest();
        jsonCranckRequest.getData().setType("role");
        jsonCranckRequest.getData().getAttributes().put("role", "Main Chararter");
        jsonCranckRequest.getData().getAttributes().put("description", "The criminal");
        jsonCranckRequest.getData().getRelationships().put("person", jsonPersonDataNew);
        jsonCranckRequest.updateRelation("person", jsonPersonDataNew);
        jsonCranckRequest.updateRelation("movie", jsonMovie);

        String uri2 = BASE_URL + "/api/" + jsonCranckRequest.getType();
        HttpEntity<JsonCranckRequest> entity0 = new HttpEntity(jsonCranckRequest, RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrnkFullData> response0 = restTemplate.exchange(uri2, HttpMethod.POST, entity0, JsonCrnkFullData.class);
        assertEquals(201, response0.getStatusCodeValue());
        assertNotNull(response0.getBody());


        HttpEntity entity = new HttpEntity(RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<JsonCrnkFilterResponse> response = restTemplate.exchange(uri2, HttpMethod.GET, entity, JsonCrnkFilterResponse.class);

        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response.getBody());
    }


    @Test
    public void testInvalidPathUnauth() {
        final String uri = BASE_URL + "/api/undefined";
        HttpEntity<Void> entity0 = new HttpEntity(RestResponseHandler.createHeadersCrnk("user", "password"));
        ResponseEntity<ErrorResponse> response0 = restTemplate.exchange(uri, HttpMethod.POST, entity0, ErrorResponse.class);
        assertEquals(403, response0.getStatusCodeValue());
        assertNotNull(response0.getBody());

        assertEquals(403, response0.getBody().getStatus().intValue());
        assertEquals("Forbidden", response0.getBody().getMessage());
        assertEquals("Forbidden", response0.getBody().getError());
        assertEquals("/api/undefined", response0.getBody().getPath());
    }

    @Test
    public void testInvalidPathUnauthBobo() {
        final String uri = BASE_URL + "/api/undefined";

        HttpEntity<Void> entity0 = new HttpEntity(RestResponseHandler.createHeadersCrnk("bobo", "password"));
        ResponseEntity<ErrorResponse> response0 = restTemplate.exchange(uri, HttpMethod.POST, entity0, ErrorResponse.class);
        assertEquals(401, response0.getStatusCodeValue());
        assertNotNull(response0.getBody());

        assertEquals(401, response0.getBody().getStatus().intValue());
        assertEquals("Unauthorized", response0.getBody().getMessage());
        assertEquals("Unauthorized", response0.getBody().getError());
        assertEquals("/api/undefined", response0.getBody().getPath());
    }

    @Test
    public void testInvalidPathAdmin() {
        final String uri = BASE_URL + "/api/undefined";
        HttpEntity<Void> entity0 = new HttpEntity(RestResponseHandler.createHeadersCrnk("admin", "password"));
        ResponseEntity<ErrorResponse> response0 = restTemplate.exchange(uri, HttpMethod.POST, entity0, ErrorResponse.class);
        assertEquals(404, response0.getStatusCodeValue());
        assertNotNull(response0.getBody());

        assertEquals(404, response0.getBody().getStatus().intValue());
        assertEquals("No message available", response0.getBody().getMessage());
        assertEquals("Not Found", response0.getBody().getError());
        assertEquals("/api/undefined", response0.getBody().getPath());
    }


    @Test
    public void testInvalidActionAdmin() {
        final String uri = BASE_URL + "/api/role";

        GenericResult<JsonCrnkFullData> responseInfo = RestResponseHandler.crnkPostDataHandler(uri, null, "admin", "password", JsonCrnkFullData.class);
        assertNotNull(responseInfo);
        assertNotNull(responseInfo.getErrorResponse());
        assertEquals(400, responseInfo.getStatus());
        assertEquals("Request body not found", responseInfo.getErrorResponse().getError());
        assertEquals("Request body not found, POST method, resource name role", responseInfo.getErrorResponse().getMessage());
    }

    @Test
    public void addLocationWithData() {
        JsonCranckRequest jsonCranckRequest = new JsonCranckRequest();
        jsonCranckRequest.setType("address");
        jsonCranckRequest.setvalue("zipcode", "2626DF");
        jsonCranckRequest.setvalue("number", 855);
        jsonCranckRequest.setvalue("extension", "");
        jsonCranckRequest.setvalue("city", "Delft");
        jsonCranckRequest.setvalue("streetname", "Roland Holstlaan");

        final String uri = BASE_URL + "/api/address";

        GenericResult<JsonCrnkFullData> responseInfo = RestResponseHandler.crnkPostDataHandler(uri, jsonCranckRequest, "admin", "password", JsonCrnkFullData.class);
        assertNotNull(responseInfo);
        assertNotNull(responseInfo.getResultEnitity());

        JsonCrnkFullData result = responseInfo.getResultEnitity();
        assertNotNull(result);
        assertEquals(jsonCranckRequest.getData().getAttributes().get("number"), result.getData().getAttributes().get("number"));
        assertEquals(jsonCranckRequest.getData().getAttributes().get("zipcode"), result.getData().getAttributes().get("zipcode"));
        assertEquals(jsonCranckRequest.getData().getAttributes().get("streetname"), result.getData().getAttributes().get("streetname"));
    }

    @Test
    public void z_einde01_ForClosingContext() {
        final String uri = BASE_URL + "/shutdownContext";

        HttpEntity<String> entity = new HttpEntity<>(RestResponseHandler.createHeadersCrnk("bobo", "password"));
        ResponseEntity<ErrorResponse> response = restTemplate.postForEntity(uri, entity, ErrorResponse.class);
        assertNotNull(response);
        assertNotNull(response.getBody());
        assertEquals("Unauthorized", response.getBody().getError());
        assertEquals("Unauthorized", response.getBody().getMessage());
        assertEquals(401, response.getStatusCodeValue());
    }

    @Test
    public void z_einde02_ForClosingContext() {
        final String uri = BASE_URL + "/shutdownContext";

        HttpEntity<String> entity = new HttpEntity<>(RestResponseHandler.createHeadersCrnk("user", "password"));
        ResponseEntity<ErrorResponse> response = restTemplate.postForEntity(uri, entity, ErrorResponse.class);
        assertNotNull(response);
        assertNotNull(response.getBody());
        assertEquals("Forbidden", response.getBody().getError());
        assertEquals("Forbidden", response.getBody().getMessage());
        assertEquals(403, response.getStatusCodeValue());
    }

    @Test
    public void z_einde03_ForClosingContext() {
        final String uri = BASE_URL + "/shutdownContext";

        expectedException.expect(ResourceAccessException.class);
        expectedException.expectMessage(String.format("I/O error on POST request for \"%s\"", uri));

        HttpEntity<String> entity = new HttpEntity<>(RestResponseHandler.createHeadersCrnk("admin", "password"));
        restTemplate.postForEntity(uri, entity, String.class);
    }


    @Test
    public void z_einde04_AfterClosingContext() {
        final String uri = BASE_URL + "/postdummy.json";

        expectedException.expect(ResourceAccessException.class);
        expectedException.expectMessage(String.format("I/O error on POST request for \"%s\"", uri));
        HttpEntity<String> entity = new HttpEntity<>(RestResponseHandler.createHeadersCrnk("admin", "password"));
        restTemplate.postForEntity(uri, entity, String.class);

    }


}
