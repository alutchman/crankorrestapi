# README #

* Spring Boot JSON Rest API
* JPA with Hibernate
* Entities require JPARespostiories

* 2 ways of setting up a Json Api
    - Rest Json
    - Crank Json

Both make it easy to interact with the database.
With Crank only entities are required. With the Data Rest Json we also need 
Repositories (JPA) for the enetities.

Both will require de DataBase Configuration.

With Sonar you may test and optimize the project. We found this application
to work well with Spring Boot version 2.0.4-RELEASE. The previous version
resulted in errors with unit testing.

REQUIRED:

    <repositories>
        <repository>
            <id>crnk_IO</id>
            <name>Crank api Repository</name>
            <url>https://dl.bintray.com/crnk-project/maven/</url>
        </repository>
    </repositories>
    
    
    
Recommended : https://www.oreilly.com/library/view/restful-web-services/9780596529260/ch04.html
