package org.apps.base01.dbtables.tables;

import io.crnk.core.resource.annotations.JsonApiResource;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@JsonApiResource(type = "person")
@Data
@Entity
@Table(
		name="person_tbl"
)
@EqualsAndHashCode(exclude={"year"})
public class PersonEntity {

	@Id
	private UUID id;

	private String name;

	private int year;

	@OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
	private List<RoleType> roleTypes = new ArrayList<>();

	@Version
	private Integer version;

}
