package org.apps.base01.dbtables.tables;

import io.crnk.core.resource.annotations.JsonApiResource;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@JsonApiResource(type = "movie")
@Data
@Entity
@Table(
		name="movie_tbl"
)
@EqualsAndHashCode(exclude={"year"})
public class MovieEntity {

	@Id
	private UUID id;

	private String name;

	private int year;

	@OneToMany(mappedBy = "movie")
	private List<RoleType> roles = new ArrayList<>();

	@Version
	private Integer version;


}
