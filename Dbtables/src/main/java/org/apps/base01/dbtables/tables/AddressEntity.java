package org.apps.base01.dbtables.tables;

import io.crnk.core.resource.annotations.JsonApiResource;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@JsonApiResource(type = "address")
@Data
@Entity
@Table(
        name="address_tbl",
        uniqueConstraints=
        @UniqueConstraint(columnNames={"zipcode", "number", "extension"})
)
public class AddressEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    protected Long id;

    @Column( name = "streetname",nullable = false, length = 60)
    private String streetname;

    @Column( name = "number",nullable = false)
    private Integer number;

    @Column( name = "extension",nullable = false, length=10)
    private String extension;

    @Column( name = "city",nullable = false, length = 25)
    private String city;

    @Column( name = "zipcode",nullable = false, length = 8)
    private String zipcode;

    @Column( name = "phone", length = 20)
    private String phone;


}
