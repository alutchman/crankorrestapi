package org.apps.base01.dbtables.tables;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.crnk.core.resource.annotations.JsonApiResource;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@JsonApiResource(type = "role")
@Data
@Entity
@Table(
		name="role_tbl"
)
@EqualsAndHashCode(exclude={"someId"})
public class RoleType {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;

	@JsonProperty("role")
	private String name;

	@Size(max = 35, message = "Description may not exceed {max} characters.")
	private String description;


	private Long someId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "movie_id")
	private MovieEntity movie;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "person_id")
	private PersonEntity person;

}
