package org.apps.base01.datarest.data.repo;

import java.util.List;
import org.apps.base01.dbtables.tables.AddressEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource(collectionResourceRel = "addresses", path = "address")
public interface AdressRepository extends JpaRepository<AddressEntity, Long> {

    List<AddressEntity> findByZipcode(@Param("zipcode") String value);

    @Query("SELECT t FROM AddressEntity t where zipcode=?1 AND  number=?2")
    List<AddressEntity> findByZipcodeNumber(@Param("zipcode") String zipcode, @Param("number") int number);


}
