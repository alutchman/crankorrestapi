package org.apps.base01.datarest.data.repo;

import org.apps.base01.dbtables.tables.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "roles", path = "role")
public interface RoleRepo extends JpaRepository<RoleType, Long> {

}
