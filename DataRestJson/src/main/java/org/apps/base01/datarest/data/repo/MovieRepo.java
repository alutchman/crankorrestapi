package org.apps.base01.datarest.data.repo;

import java.util.List;
import java.util.UUID;

import org.apps.base01.dbtables.tables.MovieEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "movies", path = "movie")
public interface MovieRepo extends JpaRepository<MovieEntity, UUID> {

    List<MovieEntity> findByName(String name);

    List<MovieEntity> findByYear(int year);

}
