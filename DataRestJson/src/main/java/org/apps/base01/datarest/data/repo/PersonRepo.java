package org.apps.base01.datarest.data.repo;

import java.util.UUID;

import org.apps.base01.dbtables.tables.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "persons", path = "person")
public interface PersonRepo extends JpaRepository<PersonEntity, UUID> {

}
