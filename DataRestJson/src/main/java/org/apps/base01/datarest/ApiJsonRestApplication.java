package org.apps.base01.datarest;


import org.apps.base01.helperutils.config.DataBaseConfiguration;
import org.apps.base01.helperutils.config.SpringSecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan({"org.apps.base01.helperutils.controllers"})
@Import({ DataBaseConfiguration.class, SpringSecurityConfig.class})
@SpringBootApplication
@EnableJpaRepositories(basePackages = {
        "org.apps.base01.datarest.data.repo"
})
public class ApiJsonRestApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiJsonRestApplication.class, args);
    }
}
