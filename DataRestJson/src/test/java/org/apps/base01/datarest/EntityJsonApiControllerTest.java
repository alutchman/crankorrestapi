package org.apps.base01.datarest;

import java.util.List;

import org.apps.base01.datarest.data.messages.AdressResponse;
import org.apps.base01.datarest.data.messages.ExceptionCauseResponse;
import org.apps.base01.datarest.data.messages.ListResponse;
import org.apps.base01.datarest.data.messages.RootResponse;
import org.apps.base01.datarest.testutils.TestDataCreator;
import org.apps.base01.helperutils.interfacing.RestResponseHandler;
import org.apps.base01.helperutils.responses.ErrorResponse;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;
import static org.apps.base01.helperutils.utils.JsonConvertUtility.getJsonObject;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@Slf4j
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EntityJsonApiControllerTest {
    private static final int PORTNUMBER = 65532;
    private static final String PORTARG = "--server.port="+PORTNUMBER;
    private static final String BASE_URL = "http://localhost:"+PORTNUMBER;

    private static AdressResponse result1;
    private static AdressResponse result2;
    private static AdressResponse result3;
    private static List<AdressResponse> resultList;

    private static RestTemplate restTemplate = RestResponseHandler.getRestTemplate();

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @BeforeClass
    public static void init(){
        String[] args = new String[]{PORTARG};
        ApiJsonRestApplication.main(args);
    }

    @Before
    public void befoteTest(){
        if (resultList == null) {
            resultList = TestDataCreator.getCreatedTestData(BASE_URL +"/address", restTemplate);
            result1 = resultList.get(0);
            result2 = resultList.get(1);
            result3 = resultList.get(2);
        }
    }


    @Test
    public void test000_GetDefinedID0_AllEntities(){
        final String uri = BASE_URL+"/";

        HttpEntity<Void> request = new HttpEntity<>(RestResponseHandler.createHeadersJson("user", "password"));
        ResponseEntity<RootResponse> response = restTemplate.exchange(uri, HttpMethod.GET, request, RootResponse.class);

        assertNotNull(response.getBody().get_links().get("addresses"));
        assertNotNull(response.getBody().get_links().get("movies"));
        assertNotNull(response.getBody().get_links().get("roles"));
        assertNotNull(response.getBody().get_links().get("persons"));
    }

    @Test
    public void test001_GetDefinedID1(){
        String uri = BASE_URL +"/address/1";

        HttpEntity<Void> request = new HttpEntity<>(RestResponseHandler.createHeadersJson("user", "password"));
        ResponseEntity<AdressResponse> response = restTemplate.exchange(uri, HttpMethod.GET, request, AdressResponse.class);

        assertNotNull(response);
        assertNotNull(response.getBody());
    }


    @Test
    public void test001_GetDefinedID2(){
        String uri = BASE_URL +"/address/2";

        HttpEntity<Void> request = new HttpEntity<>(RestResponseHandler.createHeadersJson("user", "password"));
        ResponseEntity<AdressResponse> response = restTemplate.exchange(uri, HttpMethod.GET, request, AdressResponse.class);

        assertNotNull(response);
        assertNotNull(response.getBody());
    }


    @Test
    public void test001_FindByParams0(){
        String uri = BASE_URL +"/address/search/findByZipcode?zipcode=3000AH";

        HttpEntity<Void> request = new HttpEntity<>(RestResponseHandler.createHeadersJson("user", "password"));
        ResponseEntity<ListResponse> response = restTemplate.exchange(uri, HttpMethod.GET, request, ListResponse.class);

        assertNotNull(response);
        assertNotNull(response.getBody());

        ListResponse<AdressResponse> resultList = response.getBody();
        assertNotNull(resultList);
        assertNotNull(resultList.get_embedded().get("addresses"));
        List<AdressResponse> lstAdresses = (List<AdressResponse>) resultList.get_embedded().get("addresses");
        assertEquals(0, lstAdresses.size());
    }

    @Test
    public void test001_FindByParams1(){
        String uri = BASE_URL +"/address/search/findByZipcode?zipcode=2525KH";

        HttpEntity<Void> request = new HttpEntity<>(RestResponseHandler.createHeadersJson("user", "password"));
        ResponseEntity<ListResponse> response = restTemplate.exchange(uri, HttpMethod.GET, request, ListResponse.class);

        assertNotNull(response);
        assertNotNull(response.getBody());

        ListResponse<AdressResponse> resultList = response.getBody();
        assertNotNull(resultList);
        assertNotNull(resultList.get_embedded().get("addresses"));
        List<AdressResponse> lstAdresses = (List<AdressResponse>) resultList.get_embedded().get("addresses");
        assertEquals(2, lstAdresses.size());
    }

    @Test
    public void test002_DeleteValidId(){
        String urlSearch = BASE_URL + "/address/search/findByZipcodeNumber?zipcode=2525KH&number=6";

        HttpEntity<Void> request = new HttpEntity<>(RestResponseHandler.createHeadersJson("user", "password"));
        ResponseEntity<ListResponse> response = restTemplate.exchange(urlSearch, HttpMethod.GET, request, ListResponse.class);

        assertNotNull(response);
        assertNotNull(response.getBody());

        ListResponse<AdressResponse> resultList = response.getBody();
        assertNotNull(resultList);
        assertNotNull(resultList.get_embedded().get("addresses"));
        List<AdressResponse> lstAdresses = (List<AdressResponse>) resultList.get_embedded().get("addresses");
        assertEquals(1, lstAdresses.size());


        String targetUrl = BASE_URL+ "/address/4";;
        request = new HttpEntity<>(RestResponseHandler.createHeadersJson("admin", "password"));
        ResponseEntity<String> result0 = restTemplate.exchange(targetUrl, HttpMethod.DELETE, request, String.class);
        assertEquals(204, result0.getStatusCodeValue());
        assertNull(result0.getBody());

        request = new HttpEntity<>(RestResponseHandler.createHeadersJson("user", "password"));
        response = restTemplate.exchange(urlSearch, HttpMethod.GET, request, ListResponse.class);

        resultList = response.getBody();
        assertNotNull(resultList);
        assertNotNull(resultList.get_embedded().get("addresses"));
        lstAdresses = (List<AdressResponse>) resultList.get_embedded().get("addresses");
        assertEquals(0, lstAdresses.size());
    }


    @Test
    public void test002_DeleteInvalidId(){
        String targetUrl = BASE_URL+ "/address/111";
        HttpEntity<String> request = new HttpEntity<String>(RestResponseHandler.createHeadersJson("admin", "password"));
        ResponseEntity<String> result0 = restTemplate.exchange(targetUrl, HttpMethod.DELETE, request, String.class);
        assertEquals(404, result0.getStatusCodeValue());
        assertNull(result0.getBody());
    }


    @Test
    public void test003_InvalidService(){
        String targetUrl = BASE_URL+ "/unknown/1";

        HttpEntity<Void> request = new HttpEntity<>(RestResponseHandler.createHeadersJson("user", "password"));
        ResponseEntity<ErrorResponse> response = restTemplate.exchange(targetUrl, HttpMethod.GET, request, ErrorResponse.class);
        assertEquals(404, response.getStatusCodeValue());
        ErrorResponse errorResponse = response.getBody();

        assertNotNull(errorResponse);
        assertEquals(404, errorResponse.getStatus().intValue());
        assertEquals("No message available", errorResponse.getMessage());
        assertEquals("Not Found", errorResponse.getError());
    }

    @Test
    public void test005_InvalidGetLocatie(){
        String targetUrl =  BASE_URL +"/api/locatie/bla ";

        HttpEntity<Void> request = new HttpEntity<>(RestResponseHandler.createHeadersJson("user", "password"));
        ResponseEntity<ErrorResponse> response = restTemplate.exchange(targetUrl, HttpMethod.GET, request, ErrorResponse.class);
        ErrorResponse errorResponse = response.getBody();

        assertNotNull(errorResponse);
        assertEquals(404, errorResponse.getStatus().intValue());
        assertEquals("No message available", errorResponse.getMessage());
        assertEquals("Not Found", errorResponse.getError());
    }

    @Test
    public void test005_InvalidGetLocatieNoOne(){
        String targetUrl =  BASE_URL +"/api/locatie/bla";

        HttpEntity<String> request = new HttpEntity<String>(RestResponseHandler.createHeadersJson("bobo", "password"));

        ResponseEntity<ErrorResponse> response = restTemplate.postForEntity(targetUrl, request, ErrorResponse.class);
        assertNotNull(response.getBody());
        log.info(response.getBody().toString());

        ErrorResponse errorResponse = response.getBody();
        assertNotNull(errorResponse);
        assertEquals(401,  errorResponse.getStatus().intValue());
        assertEquals("Unauthorized",  errorResponse.getMessage());
        assertEquals("Unauthorized",  errorResponse.getError());
        assertEquals("/api/locatie/bla",  errorResponse.getPath());
    }

    @Test
    public void test006_findNoAdress() {
        String targetUrl = BASE_URL+ "/address/5";
        HttpEntity<Void> request = new HttpEntity<>(RestResponseHandler.createHeadersJson("user", "password"));
        ResponseEntity<ErrorResponse> response = restTemplate.exchange(targetUrl, HttpMethod.GET, request, ErrorResponse.class);

        assertEquals(404, response.getStatusCodeValue());
        assertNull(response.getBody());
    }

   @Test
    public void test007_post404Error() {
        final String uri = BASE_URL +"/postdummy.json";
        HttpEntity<String> entity = new HttpEntity<>(RestResponseHandler.createHeadersJson("user", "password"));
        ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);
        assertNotNull(response.getBody());
        String responstTxt = response.getBody();
        log.info(responstTxt);

        ErrorResponse result = getJsonObject(responstTxt, ErrorResponse.class);
        assertNotNull(result);
        assertEquals("No message available", result.getMessage());
        assertEquals(404, result.getStatus().intValue());
    }


    @Test
    public void test009_PatchAsdresObjectOK(){
        String uri =  BASE_URL +"/address/1";

        HttpEntity<Void> request = new HttpEntity<>(RestResponseHandler.createHeadersJson("user", "password"));
        ResponseEntity<AdressResponse> response = restTemplate.exchange(uri, HttpMethod.GET, request, AdressResponse.class);
        AdressResponse adressResponse = response.getBody();
        assertNotNull(adressResponse);
        assertNull(adressResponse.getPhone());
        adressResponse.setPhone("0123456789");

        HttpEntity<AdressResponse> requestEntity = new HttpEntity<>(adressResponse,
                RestResponseHandler.createHeadersJson("admin", "password"));
        ResponseEntity<AdressResponse> responsexx =
                restTemplate.exchange(uri, HttpMethod.PATCH, requestEntity, AdressResponse.class);

        response = restTemplate.exchange(uri, HttpMethod.GET, request, AdressResponse.class);
        AdressResponse adressResponse1 = response.getBody();
        assertNotNull(adressResponse1);
        assertNotNull(adressResponse1.getPhone());
        assertEquals("0123456789", adressResponse1.getPhone());
    }

    @Test
    public void test010_PatchAsdresObjectOK(){
        String uri =  BASE_URL +"/address/1";
        HttpEntity<Void> request = new HttpEntity<>(RestResponseHandler.createHeadersJson("user", "password"));
        ResponseEntity<AdressResponse> response = restTemplate.exchange(uri, HttpMethod.GET, request, AdressResponse.class);
        AdressResponse adressResponse = response.getBody();
        assertNotNull(adressResponse);

        adressResponse.setPhone("01040012345");

        HttpEntity<AdressResponse> requestEntity = new HttpEntity<>(adressResponse,
                RestResponseHandler.createHeadersJson("admin", "password"));
        ResponseEntity<AdressResponse> responsexx =
                restTemplate.exchange(uri, HttpMethod.PATCH, requestEntity, AdressResponse.class);
        AdressResponse adressResponse1 = responsexx.getBody();

        assertNotNull(adressResponse1);
        assertNotNull(adressResponse1.getPhone());
        assertEquals("01040012345", adressResponse1.getPhone());
    }


    @Test
    public void test011_PatchAsdresObjectNotOK(){
        String uri =  BASE_URL +"/address/1";
        HttpEntity<Void> request = new HttpEntity<>(RestResponseHandler.createHeadersJson("user", "password"));
        ResponseEntity<AdressResponse> response = restTemplate.exchange(uri, HttpMethod.GET, request, AdressResponse.class);
        AdressResponse adressResponse = response.getBody();
        assertNotNull(adressResponse);

        adressResponse.setZipcode("2907DD");
        adressResponse.setNumber(11);
        adressResponse.setExtension("bis");
        adressResponse.setPhone("0123456789");

        HttpEntity<AdressResponse> requestEntity = new HttpEntity<>(adressResponse,
                RestResponseHandler.createHeadersJson("admin", "password"));
        ResponseEntity<ExceptionCauseResponse> responsexx =
                restTemplate.exchange(uri, HttpMethod.PATCH, requestEntity, ExceptionCauseResponse.class);
        assertEquals(409, responsexx.getStatusCodeValue());

        ExceptionCauseResponse exceptionCauseResponse = responsexx.getBody();
        assertNotNull(exceptionCauseResponse);
        assertTrue(exceptionCauseResponse.getMessage().startsWith("could not execute statement; SQL [n/a];"));
        assertNull(exceptionCauseResponse.getCause().getCause().getCause());
        assertTrue(exceptionCauseResponse.getCause().getCause().getMessage().startsWith("Unique index or primary key violation: "));
    }


    @Test
    public void test012_PatchAsdresObjectInvalidID(){
        String uri =  BASE_URL  +"/address/1";
        HttpEntity<Void> request = new HttpEntity<>(RestResponseHandler.createHeadersJson("user", "password"));
        ResponseEntity<AdressResponse> response = restTemplate.exchange(uri, HttpMethod.GET, request, AdressResponse.class);
        AdressResponse adressResponse = response.getBody();

        assertNotNull(adressResponse);

        adressResponse.setZipcode("2907DD");
        adressResponse.setNumber(11);
        adressResponse.setExtension("bis");
        adressResponse.setPhone("0123456789");

        String InvaliudUri =  BASE_URL +"/address/99991";

        HttpEntity<AdressResponse> requestEntity = new HttpEntity<>(adressResponse,
                RestResponseHandler.createHeadersJson("admin", "password"));
        ResponseEntity<AdressResponse> responsexx =
                restTemplate.exchange(InvaliudUri, HttpMethod.PATCH, requestEntity, AdressResponse.class);
        assertEquals(404, responsexx.getStatusCodeValue());
    }
    @Test
    public void test101_ForClosingContext() {
        final String uri = BASE_URL +"/shutdownContext";

        HttpEntity<String> entity = new HttpEntity<>(RestResponseHandler.createHeadersJson("bobo", "password"));
        ResponseEntity<ErrorResponse> response = restTemplate.postForEntity(uri, entity, ErrorResponse.class);
        assertNotNull(response);
        assertNotNull(response.getBody());
        assertEquals("Unauthorized", response.getBody().getError());
        assertEquals("Unauthorized", response.getBody().getMessage());
        assertEquals(401, response.getStatusCodeValue());

    }

    @Test
    public void test102_ForClosingContext() {
        final String uri = BASE_URL +"/shutdownContext";

        HttpEntity<String> entity = new HttpEntity<>(RestResponseHandler.createHeadersJson("user", "password"));
        ResponseEntity<ErrorResponse> response = restTemplate.postForEntity(uri, entity, ErrorResponse.class);
        assertNotNull(response);
        assertNotNull(response.getBody());
        assertEquals("Forbidden", response.getBody().getError());
        assertEquals("Forbidden", response.getBody().getMessage());
        assertEquals(403, response.getStatusCodeValue());
    }

    @Test
    public void test103_ForClosingContext() {
        final String uri = BASE_URL +"/shutdownContext";
        expectedException.expect(ResourceAccessException.class);
        expectedException.expectMessage(String.format("I/O error on POST request for \"%s\"", uri));

        HttpEntity<String> entity = new HttpEntity<>(RestResponseHandler.createHeadersJson("admin", "password"));
        restTemplate.postForEntity(uri, entity, String.class);
    }


    @Test
    public void test105_AfterClosingContext() {
        final String uri = BASE_URL +"/postdummy.json";

        expectedException.expect(ResourceAccessException.class);
        expectedException.expectMessage(String.format("I/O error on POST request for \"%s\"", uri));

        HttpEntity<String> entity = new HttpEntity<>(RestResponseHandler.createHeadersJson("admin", "password"));
        restTemplate.postForEntity(uri, entity, String.class);
    }


}