package org.apps.base01.datarest.data.messages;



import org.apps.base01.datarest.data.HrefForLinksTemplated;

import java.util.HashMap;
import java.util.Map;

public class BaseResponse {
    private Map<String, HrefForLinksTemplated> _links = new HashMap<>();

    public Map<String, HrefForLinksTemplated> get_links() {
        return _links;
    }

    public void set_links(Map<String, HrefForLinksTemplated> _links) {
        this._links = _links;
    }
}
