package org.apps.base01.datarest.data.messages;

import org.apps.base01.datarest.data.HrefForLinksTemplated;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListResponse<E>  {
    private Map<String,List<? super E>> _embedded;

    private Map<String, HrefForLinksTemplated> _links = new HashMap<>();

    public Map<String, HrefForLinksTemplated> get_links() {
        return _links;
    }

    public void set_links(Map<String, HrefForLinksTemplated> _links) {
        this._links = _links;
    }

    public Map<String, List<? super E>> get_embedded() {
        return _embedded;
    }

    public void set_embedded(Map<String, List<? super E>> _embedded) {
        this._embedded = _embedded;
    }
}
