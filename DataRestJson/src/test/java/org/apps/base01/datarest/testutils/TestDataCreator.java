package org.apps.base01.datarest.testutils;

import java.util.ArrayList;
import java.util.List;

import org.apps.base01.datarest.data.messages.AdressRequest;
import org.apps.base01.datarest.data.messages.AdressResponse;
import org.apps.base01.helperutils.interfacing.RestResponseHandler;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestDataCreator {
    private static AdressResponse handleAddAdress(String uri, RestTemplate restTemplate, AdressRequest data){
        HttpEntity<AdressRequest> requestEntity = new HttpEntity<>(data, RestResponseHandler.createHeadersJson("admin", "password"));

        ResponseEntity<AdressResponse> response =
                restTemplate.exchange(uri, HttpMethod.POST, requestEntity, AdressResponse.class);
        return response.getBody();
    }

    public static AdressResponse addLocatie1(String uri, RestTemplate restTemplate){
        AdressRequest data = new AdressRequest("2806EB", 55, "", "Gouda",
                "Poldermolenerf", null);

        return handleAddAdress(uri, restTemplate, data);
    }


    public static AdressResponse addLocatie2(String uri, RestTemplate restTemplate){
        AdressRequest data = new AdressRequest("2907DD", 11, "bis", "Capelle aan den IJssel",
                "Grafiek", null);

        return handleAddAdress(uri, restTemplate, data);
    }


    public static AdressResponse addLocatie3(String uri, RestTemplate restTemplate){
        AdressRequest data = new AdressRequest("2525KH", 4, "", "Den Haag",
                "Fruitweg", null);


        return handleAddAdress(uri, restTemplate, data);
    }

    public static AdressResponse addLocatie4(String uri, RestTemplate restTemplate){
        AdressRequest data = new AdressRequest("2525KH", 6, "", "Den Haag",
                "Fruitweg", null);

        return handleAddAdress(uri, restTemplate, data);
    }

    public static List<AdressResponse> getCreatedTestData(String uri, RestTemplate restTemplate){
        List<AdressResponse> resultList = new ArrayList<>();
        resultList.add(addLocatie1(uri, restTemplate));
        resultList.add(addLocatie2(uri, restTemplate));
        resultList.add(addLocatie3(uri, restTemplate));
        resultList.add(addLocatie4(uri, restTemplate));

        return resultList;
    }
}
