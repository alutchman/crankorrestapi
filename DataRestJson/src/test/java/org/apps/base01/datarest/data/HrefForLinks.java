package org.apps.base01.datarest.data;

public class HrefForLinks {
    private String href;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
