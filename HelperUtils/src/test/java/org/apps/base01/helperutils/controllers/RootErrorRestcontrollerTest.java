package org.apps.base01.helperutils.controllers;

import lombok.extern.slf4j.Slf4j;
import org.apps.base01.helperutils.responses.ErrorResponse;
import org.apps.base01.helperutils.utils.JsonConvertUtility;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.context.request.WebRequest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class RootErrorRestcontrollerTest {
    @Mock
    public ErrorAttributes errorAttributes;

    @InjectMocks
    public RootErrorRestcontroller rootErrorRestcontroller;

    @Before
    public void BeforeTesting(){
        ReflectionTestUtils.setField(rootErrorRestcontroller, "errorPath", "/error");
        ReflectionTestUtils.setField(rootErrorRestcontroller, "timestampKey", "timestamp");
        ReflectionTestUtils.setField(rootErrorRestcontroller, "statusKey", "status");
        ReflectionTestUtils.setField(rootErrorRestcontroller, "errorKey", "error");
        ReflectionTestUtils.setField(rootErrorRestcontroller, "messageKey", "message");
        ReflectionTestUtils.setField(rootErrorRestcontroller, "pathKey", "path");
    }

    @Test
    public void error() {
        Map<String, Object> errorInfo = new HashMap<>();
        errorInfo.put("timestamp", new Date());
        errorInfo.put("status", 404);
        errorInfo.put("error", "Not Found");
        errorInfo.put("message", "No message available");
        errorInfo.put("path", "/postdummy.json");
        WebRequest webRequest = mock(WebRequest.class);

        when(errorAttributes.getErrorAttributes(webRequest, false)).thenReturn(errorInfo);
        ErrorResponse errorResponse = rootErrorRestcontroller.error(webRequest);
        String errorResponseStr = JsonConvertUtility.getJsonAsString(errorResponse);
        log.info(errorResponseStr);
        assertEquals(errorInfo.get("status"), errorResponse.getStatus());
        assertEquals(errorInfo.get("error"), errorResponse.getError());
        assertEquals(errorInfo.get("message"), errorResponse.getMessage());
        assertEquals(errorInfo.get("path"), errorResponse.getPath());
    }

    @Test
    public void getErrorPath() {
        String errorPath = rootErrorRestcontroller.getErrorPath();
        assertEquals("/error", errorPath);
    }
}