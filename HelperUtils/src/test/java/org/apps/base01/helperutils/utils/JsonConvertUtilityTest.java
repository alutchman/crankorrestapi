package org.apps.base01.helperutils.utils;

import lombok.extern.slf4j.Slf4j;
import org.apps.base01.helperutils.responses.ErrorResponse;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@Slf4j
public class JsonConvertUtilityTest {
    private int status = 404;
    private Date timeStamp = new Date();
    private String error = "Something Wong";
    private String message = "Test item no errors here";
    private String path = "/api/somepath";

    @Test
    public void getJsonAsStringHappyFlow() {
        Map<String,Object> errorResponseMap = new HashMap<>();

        errorResponseMap.put("status", status);
        errorResponseMap.put("timestamp",timeStamp);
        errorResponseMap.put("error", error);
        errorResponseMap.put("message", message);
        errorResponseMap.put("path", path);


        ErrorResponse errorResponse = new ErrorResponse(errorResponseMap, "status",
                "timestamp", "error",
                "message",  "path");
        assertEquals(status,(int) errorResponse.getStatus());
        assertEquals(timeStamp,  errorResponse.getTimestamp());
        assertEquals(error,  errorResponse.getError());
        assertEquals(message,  errorResponse.getMessage());
        assertEquals(path,  errorResponse.getPath());
        String serialError = JsonConvertUtility.getJsonAsString(errorResponse);
        assertNotNull(serialError);

        log.info(serialError);

    }


    @Test
    public void getJsonAsStringUnHappyFlow() {
        Map<String,Object> errorResponseMap = new HashMap<>();

        errorResponseMap.put("status", status);
        errorResponseMap.put("timestamp",timeStamp);
        errorResponseMap.put("error", error);
        errorResponseMap.put("message", message);
        errorResponseMap.put("path", path);

        String serialError = JsonConvertUtility.getJsonAsString(new Object());
        assertNull(serialError);
    }

    @Test
    public void getJsonObjectHappyFlow() {
        String errorStr = "{\n" +
                "  \"status\" : 404,\n" +
                "  \"timestamp\" : \"2018-08-15T16:38:26.311Z\",\n" +
                "  \"error\" : \"Something Wong\",\n" +
                "  \"message\" : \"Test item no errors here\",\n" +
                "  \"path\" : \"/api/somepath\"\n" +
                "}";
        ErrorResponse errorResponse = JsonConvertUtility.getJsonObject(errorStr, ErrorResponse.class);
        assertNotNull(errorResponse);
    }


    @Test
    public void getJsonObjectUnHappyFlow() {
        String errorStr = "{\n" +
                "  \"ztatus\" : 404,\n" +
                "  \"timeStamp\" : \"2018-08-15T16:38:26.311Z\",\n" +
                "  \"error\" : \"Something Wong\",\n" +
                "  \"message\" : \"Test item no errors here\",\n" +
                "  \"path\" : \"/api/somepath\"\n" +
                "}";
        ErrorResponse errorResponse = JsonConvertUtility.getJsonObject(errorStr, ErrorResponse.class);
        assertNull(errorResponse);
    }
}