package org.apps.base01.helperutils.config;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;


@RunWith(MockitoJUnitRunner.class)
public class DataBaseConfigurationTest {

    @InjectMocks
    DataBaseConfiguration dataBaseConfiguration;

    @Before
    public void BeforeTesting(){
        ReflectionTestUtils.setField(dataBaseConfiguration, "driverClassname", "org.h2.Driver");
        ReflectionTestUtils.setField(dataBaseConfiguration, "jdbcUrl", "jdbc:h2:mem:helperutils_rest_test");
        ReflectionTestUtils.setField(dataBaseConfiguration, "jdbcUser", "sa");
        ReflectionTestUtils.setField(dataBaseConfiguration, "jdbcPassword", "");
        ReflectionTestUtils.setField(dataBaseConfiguration, "hibernateDialect", "org.hibernate.dialect.H2Dialect");
        ReflectionTestUtils.setField(dataBaseConfiguration, "hibernateShowSql", "false");
        ReflectionTestUtils.setField(dataBaseConfiguration, "hibernateHbm2ddlAuto", "create-drop");
        ReflectionTestUtils.setField(dataBaseConfiguration, "hibernateGenerateStatistics", "false");
    }

    @Test
    public void getDataSource() {
        DataSource dataSource = dataBaseConfiguration.getDataSource();
        assertNotNull(dataSource);
    }

    @Test
    public void entityManagerFactory() {
        DataSource dataSource = mock(DataSource.class);
        LocalContainerEntityManagerFactoryBean factoryBean = dataBaseConfiguration.entityManagerFactory(dataSource);
        assertNotNull(factoryBean);
    }

    @Test
    public void transactionManager() {
        EntityManagerFactory emf = mock(EntityManagerFactory.class);
        PlatformTransactionManager transactionManager = dataBaseConfiguration.transactionManager(emf);
        assertNotNull(transactionManager);

    }
}