package org.apps.base01.helperutils.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Import({DataBaseConfiguration.class})
@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    // Create 2 users for demo
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource);

        /*
          //Next part is redundant when using default table names as shown below

          .usersByUsernameQuery("select username, password, enabled"
                        + " from users where username=?")
                .authoritiesByUsernameQuery("select username, authority "
                        + "from authorities where username=?")
                .passwordEncoder(passwordEncoder);
       */
        /*

            String encodedPw = passwordEncoder.encode("password");
            auth.inMemoryAuthentication()
                .withUser("user").password(encodedPw).roles("OPERATOR")
                .and()
                .withUser("admin").password(encodedPw).roles("OPERATOR", "ADMIN");

        */

    }

    // Secure the endpoins with HTTP Basic authentication
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST,"/shutdownContext").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/address").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/persoon").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/address").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/persoon").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/address/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/persoon/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/address/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/persoon/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.GET,"/api/**").hasAnyRole("OPERATOR", "ADMIN")
                .antMatchers(HttpMethod.POST,"/api/**").hasAnyRole("ADMIN")
                .antMatchers(HttpMethod.DELETE,"/api/**").hasRole("ADMIN")   //.permitAll()
                .antMatchers(HttpMethod.PATCH,"/api/**").hasRole("ADMIN")   //.permitAll()
                .antMatchers(HttpMethod.PUT,"/api/**").hasRole("ADMIN")   //.permitAll()
                .antMatchers(HttpMethod.GET,"/resources-info").hasAnyRole("OPERATOR", "ADMIN")
                .and()
                .csrf().disable()
                .formLogin().disable();
    }

}
