package org.apps.base01.helperutils.responses;

import lombok.Data;

@Data
public class ErrorItem {
    private int status;
    private String title;
    private String detail;
}
