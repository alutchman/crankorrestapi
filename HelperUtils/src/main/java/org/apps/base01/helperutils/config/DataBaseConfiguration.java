package org.apps.base01.helperutils.config;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

@Slf4j
@RestController
@Configuration
@EnableAutoConfiguration
@PropertySource("classpath:dbase.properties")
@EnableTransactionManagement
public class DataBaseConfiguration {
	@Value("${jdbc.driverClassName}")
	private String driverClassname;

	@Value("${jdbc.url}")
	private String jdbcUrl;

	@Value("${jdbc.username}")
	private String jdbcUser;

	@Value("${jdbc.password}")
	private String jdbcPassword;

	@Value("${hibernate.dialect}")
	private String hibernateDialect;

	@Value("${hibernate.show_sql}")
	private String hibernateShowSql;

	@Value("${hibernate.hbm2ddl.auto}")
	private String hibernateHbm2ddlAuto;

	@Value("${hibernate.generate_statistics}")
	private String hibernateGenerateStatistics;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Bean
	public BCryptPasswordEncoder passWordEncoder() {
		return new BCryptPasswordEncoder(8);
	}

	@Bean
	public DataSource getDataSource()  {
		BasicDataSource ds = new BasicDataSource();
		ds.setDriverClassName(driverClassname);
		ds.setUrl(jdbcUrl);
		ds.setUsername(jdbcUser);
		ds.setPassword(jdbcPassword);

		log.info("Loaded Datasource for url {}", jdbcUrl);
		return ds;
	}

	@Autowired
	@Bean
	public JdbcTemplate createJdbCTemplate(DataSource dataSource) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		initAccess(jdbcTemplate, dataSource);
		return jdbcTemplate;
	}

	private void initAccess(JdbcTemplate jdbcTemplate,DataSource dataSource) {
		try {
			boolean userExists = tableExist(dataSource, "users");
			boolean authoritiesExists = tableExist(dataSource, "authorities");

			String msg = StreamUtils.copyToString(
					new ClassPathResource("auth_init.sql").getInputStream(), Charset.defaultCharset());
			String[] parts = msg.split(";");

			if (!userExists && !authoritiesExists) {
				executeImmediate(dataSource, parts[0]);
				executeImmediate(dataSource, parts[1]);
				executeImmediate(dataSource, parts[2]);
			}

			userExists = tableExist(dataSource, "users");
			authoritiesExists = tableExist(dataSource, "authorities");
			if (!userExists) {
				throw new RuntimeException("No User Table");
			}
			if (!authoritiesExists) {
				throw new RuntimeException("No Authority Table");
			}

			addBaseTestUsers(jdbcTemplate);
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}


	private boolean tableExist(DataSource dataSource, String tableName) throws SQLException {
		Connection conn = dataSource.getConnection();
		boolean tExists = false;

		try (final Statement st = conn.createStatement();
			 final ResultSet rs = st.executeQuery("show tables");) {

			while (rs.next()) {
				String tName = rs.getString("TABLE_NAME").trim();
				if (tName != null && tName.equalsIgnoreCase(tableName)) {
					tExists = true;
					break;
				}
				;
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return tExists;
	}

	private void addBaseTestUsers(JdbcTemplate jdbcTemplate) throws SQLException {
		String baseUser = "insert into users (username,password,enabled) " +
				" values(?, ? , ?)";
		String baseRole = "insert into authorities (username,authority)  " +
				" values(? ,?)";

		String encodedPw = passwordEncoder.encode("password");

		jdbcTemplate.update(baseUser, "user", encodedPw, true);
		jdbcTemplate.update(baseUser, "admin", encodedPw, true);
		jdbcTemplate.update(baseRole, "user", "ROLE_OPERATOR");
		jdbcTemplate.update(baseRole, "admin", "ROLE_OPERATOR");
		jdbcTemplate.update(baseRole, "admin", "ROLE_ADMIN");
	}


	private boolean executeImmediate(DataSource dataSource, String sql) throws SQLException {
		Connection conn = dataSource.getConnection();
		try (final Statement st = conn.createStatement()) {
			return st.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			conn.close();
		}
	}


	@Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan("org.apps.base01.dbtables.tables");

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);

        Properties jpaProperties = new Properties();
        jpaProperties.setProperty("hibernate.dialect", hibernateDialect);
        jpaProperties.setProperty("hibernate.show_sql", hibernateShowSql);
        jpaProperties.setProperty("hibernate.hbm2ddl.auto", hibernateHbm2ddlAuto);
        jpaProperties.setProperty("hibernate.generate_statistics", hibernateGenerateStatistics);
        em.setJpaProperties(jpaProperties);
        return em;
    }


    @Bean
    @Autowired(required=true)
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);

        return transactionManager;
    }


}
