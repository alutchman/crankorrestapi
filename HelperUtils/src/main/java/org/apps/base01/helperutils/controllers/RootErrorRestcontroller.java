package org.apps.base01.helperutils.controllers;

import lombok.AllArgsConstructor;
import org.apps.base01.helperutils.responses.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;


@PropertySource("classpath:error.properties")
@RestController
public class RootErrorRestcontroller  implements ErrorController {
    private boolean debug = false;

    @Value("${error.url}")
    private String errorPath;

    @Value("${timestamp.key}")
    private String timestampKey;

    @Value("${status.key}")
    private String statusKey;

    @Value("${error.key}")
    private String errorKey;

    @Value("${message.key}")
    private String messageKey;

    @Value("${path.key}")
    private String pathKey;


    @Autowired
    private ErrorAttributes errorAttributes;

    @RequestMapping(value = "${error.url}")
    public ErrorResponse error(WebRequest webRequest) {
        Map<String, Object> errorInfo = errorAttributes.getErrorAttributes(webRequest, debug);
        return new ErrorResponse(errorInfo, statusKey,
                timestampKey, errorKey,
                messageKey,  pathKey);
    }

    @Override
    public String getErrorPath() {
        return errorPath;
    }
}
