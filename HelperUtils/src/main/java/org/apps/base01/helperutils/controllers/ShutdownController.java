package org.apps.base01.helperutils.controllers;

import java.text.SimpleDateFormat;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class ShutdownController implements ApplicationContextAware {

    @Autowired
    private ApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @PostConstruct
    public void configureJackson() {

        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        objectMapper.setDateFormat(sdf);
    }


    @PostMapping("/shutdownContext")
    @ResponseBody
    public ResponseEntity<String> shutdownContext(HttpSession session, @AuthenticationPrincipal User user) {
        log.info("Closing application by user : {}", user.getUsername());
        ((ConfigurableApplicationContext) context).close();
        return new ResponseEntity("Bye Bye" ,    HttpStatus.OK);
    }

    @Override
    public void setApplicationContext(ApplicationContext ctx)  {
        this.context = ctx;
    }

}
