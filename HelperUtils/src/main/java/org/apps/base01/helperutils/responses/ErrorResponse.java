package org.apps.base01.helperutils.responses;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
public class ErrorResponse implements Serializable {
    private static final long serialVersionUID = 4484355824215402683L;

    private Integer status;
    private Date timestamp;
    private String error;
    private String message;
    private String path;

    public ErrorResponse(Map<String, Object> errorInfo, String statusKey,
                         String timestampKey, String errorKey,
                         String messageKey,  String pathKey) {
        setStatus((Integer) errorInfo.get(statusKey));
        setTimestamp((Date)  errorInfo.get(timestampKey));
        setError((String)  errorInfo.get(errorKey));
        setMessage((String)  errorInfo.get(messageKey));
        setPath((String)  errorInfo.get(pathKey));
    }
}
