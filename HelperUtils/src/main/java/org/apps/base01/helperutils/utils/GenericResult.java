package org.apps.base01.helperutils.utils;

import lombok.Data;
import org.apps.base01.helperutils.responses.ErrorResponse;

@Data
public class GenericResult<T> {
    private int status;
    private T resultEnitity;
    private ErrorResponse errorResponse;

    public GenericResult(int status, T resultEnitity, Class<T> classz) {
        this.status = status;
        this.resultEnitity = resultEnitity;
    }

    public GenericResult(int status, ErrorResponse errorResponse) {
        this.status = status;
        this.resultEnitity = null;
        this.errorResponse = errorResponse;
    }
}
