package org.apps.base01.helperutils.interfacing;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.tomcat.util.codec.binary.Base64;
import org.apps.base01.helperutils.responses.ErrorItem;
import org.apps.base01.helperutils.responses.ErrorItems;
import org.apps.base01.helperutils.responses.ErrorResponse;
import org.apps.base01.helperutils.utils.GenericResult;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import static org.apps.base01.helperutils.utils.JsonConvertUtility.getJsonAsString;
import static org.apps.base01.helperutils.utils.JsonConvertUtility.getJsonObject;

public class RestResponseHandler {
    public static final String AUTHHEADERKEY = "Authorization";
    private static final int TIMEOUT = 600000;

    public static String getBasicAuth(String username, String password){
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(
                auth.getBytes(Charset.forName("US-ASCII")) );
        String authHeader = "Basic " + new String( encodedAuth );
        return authHeader;
    }

    public static <R,T> GenericResult<T> crnkPostDataHandler(String uri, R requestData, String user, String password, Class<T> responseClass) {
        ObjectMapper objectMapper = new ObjectMapper();
        HttpPost httpPost = new HttpPost(uri);
        httpPost.addHeader(AUTHHEADERKEY, getBasicAuth(user, password));
        httpPost.addHeader("Accept", "application/vnd.api+json");
        httpPost.addHeader("Content-Type", "application/vnd.api+json");

        if (requestData != null) {
            try {
                String strData  = objectMapper.writeValueAsString(requestData);
                httpPost.setEntity(new StringEntity(strData, ContentType.APPLICATION_JSON));
            } catch (JsonProcessingException e) {

            }
        }

        ErrorResponse errorResponse =  new ErrorResponse();
        JSONObject myObject = null;
        String responstTxt = null;
        int status = 0;

        try (PoolingHttpClientConnectionManager connManager
                     = new PoolingHttpClientConnectionManager();
             CloseableHttpClient client1
                     = HttpClients.custom().setConnectionManager(connManager).build();
             CloseableHttpResponse response = client1.execute(httpPost);
             InputStream inputStream = response.getEntity().getContent()) {

            status = response.getStatusLine().getStatusCode();
            responstTxt = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
            if (!responseClass.equals(String.class)) {
                myObject = new JSONObject(responstTxt);
            } else {
                return new GenericResult(status, responstTxt, String.class);
            }
        } catch (IOException | JSONException e) {

        } finally {
            if (myObject != null ) {
                try {
                    try {
                        T result = objectMapper.readValue(myObject.toString(), responseClass);
                        return new GenericResult(status, result, responseClass);
                    }  catch(UnrecognizedPropertyException e2) {
                        ErrorItems errorItems = objectMapper.readValue(myObject.toString(), ErrorItems.class);
                        if (errorItems.getErrors().size() > 0) {
                            ErrorItem errorItem = errorItems.getErrors().get(0);
                            errorResponse.setError(errorItem.getTitle());
                            errorResponse.setMessage(errorItem.getDetail());
                        }
                    }
                } catch(IOException e) {
                    errorResponse =  getJsonObject(responstTxt, ErrorResponse.class);
                }
            }
            errorResponse.setStatus(status);
            if (errorResponse.getPath() == null) {
                errorResponse.setPath(uri);
            }
            if (errorResponse.getTimestamp() == null) {
                errorResponse.setTimestamp(new Date());
            }
            return new GenericResult(status, errorResponse);
        }
    }

    public static <R,T> GenericResult<T> crnkPatchDataHandler(String uri, R requestData, String user, String password, Class<T> responseClass) {
        ObjectMapper objectMapper = new ObjectMapper();
        HttpPatch httpPatch = new HttpPatch(uri);
        httpPatch.addHeader(AUTHHEADERKEY, getBasicAuth(user, password));
        httpPatch.addHeader("Accept", "application/vnd.api+json");
        httpPatch.addHeader("Content-Type", "application/vnd.api+json");

        if (requestData != null) {
            try {
                String strData  = objectMapper.writeValueAsString(requestData);

                httpPatch.setEntity(new StringEntity(strData, ContentType.APPLICATION_JSON));
            } catch (JsonProcessingException e) {

            }
        }

        ErrorResponse errorResponse =  new ErrorResponse();
        JSONObject myObject = null;
        String responstTxt = null;
        int status = 0;

        try (PoolingHttpClientConnectionManager connManager
                     = new PoolingHttpClientConnectionManager();
             CloseableHttpClient client1
                     = HttpClients.custom().setConnectionManager(connManager).build();
             CloseableHttpResponse response = client1.execute(httpPatch);
             InputStream inputStream = response.getEntity().getContent()) {

            status = response.getStatusLine().getStatusCode();
            responstTxt = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
            if (!responseClass.equals(String.class)) {
                myObject = new JSONObject(responstTxt);
            } else {
                return new GenericResult(status, responstTxt, String.class);
            }
        } catch (IOException | JSONException e) {

        } finally {
            if (myObject != null ) {
                try {
                    try {
                        T result = objectMapper.readValue(myObject.toString(), responseClass);
                        return new GenericResult(status, result, responseClass);
                    }  catch(UnrecognizedPropertyException e2) {
                        ErrorItems errorItems = objectMapper.readValue(myObject.toString(), ErrorItems.class);
                        if (errorItems.getErrors().size() > 0) {
                            ErrorItem errorItem = errorItems.getErrors().get(0);
                            errorResponse.setError(errorItem.getTitle());
                            errorResponse.setMessage(errorItem.getDetail());
                        }
                    }
                } catch(IOException e) {
                    errorResponse =  getJsonObject(responstTxt, ErrorResponse.class);
                }
            }
            errorResponse.setStatus(status);
            if (errorResponse.getPath() == null) {
                errorResponse.setPath(uri);
            }
            if (errorResponse.getTimestamp() == null) {
                errorResponse.setTimestamp(new Date());
            }
            return new GenericResult(status, errorResponse);
        }
    }

    public static RestTemplate getRestTemplate() {
        MappingJackson2HttpMessageConverter jackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        RestTemplate restTemplate = new RestTemplateBuilder()
                .errorHandler(new RestTemplateResponseErrorHandler())
                .build();

            final List<HttpMessageConverter<?>> messageConverters =
                    new ArrayList<HttpMessageConverter<?>>();
            messageConverters.add(new ByteArrayHttpMessageConverter());
            messageConverters.add(new ResourceHttpMessageConverter());
            messageConverters.add(new StringHttpMessageConverter());

            messageConverters.add(jackson2HttpMessageConverter);
            restTemplate.setMessageConverters(messageConverters);

            //**************** To Assure CRNK Works with PATCH you need the following Snippit of code *******
/*            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
            requestFactory.setConnectTimeout(TIMEOUT);
            requestFactory.setReadTimeout(TIMEOUT);
            restTemplate.setRequestFactory(requestFactory);*/
            //**************************************************************************************************

        return restTemplate;
    }

    public static HttpHeaders createHeadersJson(String username, String password){
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(
                auth.getBytes(Charset.forName("US-ASCII")) );
        String authHeader = "Basic " + new String( encodedAuth );
        HttpHeaders headers =  new HttpHeaders();
        headers.add("Authorization", authHeader);
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        return headers;
    }


    public static HttpHeaders createHeadersCrnk(String username, String password){
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(
                auth.getBytes(Charset.forName("US-ASCII")) );
        String authHeader = "Basic " + new String( encodedAuth );
        HttpHeaders headers =  new HttpHeaders();
        headers.add("Authorization", authHeader);
        headers.add("Accept", "application/vnd.api+json");
        headers.add("Content-Type", "application/vnd.api+json");
        return headers;
    }
}