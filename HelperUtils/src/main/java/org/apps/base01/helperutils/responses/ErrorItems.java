package org.apps.base01.helperutils.responses;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ErrorItems {
    private List<ErrorItem> errors = new ArrayList<>();
}
